import * as webpack from 'webpack';
import * as path from 'path';
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
//const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const appPath = path.join(__dirname, 'src');
const distPath = path.join(__dirname, 'dist');
const buildPath = path.join(__dirname, 'build');

const mode: any = process.env.NODE_ENV;

const config: webpack.Configuration = {
	mode: mode,
	entry: {
		aggregator: appPath + '/index.ts',
		//app: './app.ts'
	},
	module: {
		rules: [
			{
				test: /\.tsx?$/,
				use: 'ts-loader',
				exclude: /node_modules/
			}
		]
	},
	resolve: {
		extensions: ['.tsx', '.ts', '.js']
	},
	devServer: {
		compress: false,
		historyApiFallback: true,
		disableHostCheck: true,
		port: 9001,
		watchOptions: {
			ignored: /node_modules/
		},
	},

	optimization: {
		minimizer: [
			new UglifyJsPlugin({
				sourceMap: true,
				uglifyOptions: {
					compress: {
						inline: false,
					},
				},
			}),
		],
	},
	plugins: [

	],
	output: {
		filename: '[name].js',
		chunkFilename: '[id].js',
		path: buildPath,
		publicPath: '/'
	}
};

export default config;