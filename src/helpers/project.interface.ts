export interface IProjectMenuItem {
	index?: number;
	name: string;
	url: string;
	submenu: IProjectMenuItem[];
}

export interface IProject {
	name: string;
	framework: string;
	status: string;
	src: string;
	vendors: string;
	bootstrapMethod: Function;
	menu: IProjectMenuItem;
}