import { Aggregator } from './aggregator';
import { Observable, ReplaySubject } from 'rxjs/index';

export interface ISharedData {
	[name: string]: ReplaySubject<any>
}

// TODO Перенести в Loader

export class Shared {
	private data: ISharedData = {};

	constructor(
		private aggregator: Aggregator
	) {
		//
	}

	setToData(name: string, extData: any): Shared {
		if (!this.data[name]) {
			this.data[name] = new ReplaySubject<any>(1);
			this.data[name].next(extData);
		} else {
			this.data[name].next(extData)
		}

		return this;
	}

	getFromDataAsSubject(name: string): ReplaySubject<any> {
		return this.data[name];
	}

	getFromDataAsObservable(name: string): Observable<any> {
		return this.data[name].asObservable();
	}

	getData(): ISharedData {
		return this.data;
	}
}