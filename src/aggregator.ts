import * as _ from 'lodash';
import { Observable, combineLatest, Subject } from 'rxjs/index';
import { Menu } from './menu';
import { Shared } from './shared';
import { IProject } from './helpers/project.interface';

declare global {
	interface Window { Aggregator: Aggregator; }
}

export class Aggregator {
	menu: Menu = null;
	shared: Shared = null;
	project$: Observable<IProject> = null;

	constructor(
		//
	) {
		this.menu = new Menu(this);
		this.shared = new Shared(this);
		this.project$ = (window as any)['Loader'].projects.getProjectFromHashMap('aggregator');

		const projects$: Observable<any>[] = [];

		// TODO заменить на нативный for in.
		_.forIn((window as any)['Loader'].projects.getProjectsHashMap(), (value: Subject<any>, key) => {
			projects$.push(value.asObservable())
		});

		combineLatest(...projects$).subscribe((data) => {
			this.menu
				.createMenu(data)
				.mountMenu()

		});
	}
}

export const bootstrap: Function = (project, bootstrapOptions: IAngularBootstrapOptions) => {
	// platformBrowserDynamic().bootstrapModmule(bootstrapOptions.mainModule)
	// 						.then((module: NgModuleRef<AppModule>) => {
	// 							//console.log('MODULE', module);
	//
	// 							const authService: AppSharedDataService = module.injector.get(AppSharedDataService);
	//
	// 							authService.init(project.loader.shared);
	//
	// 							console.log('MODULE', module, authService);
	// 						}).catch((error) => console.log(error));
	//
	// // TODO нужен Subject или Observable
	return project
};

export const mount: Function = (project, bootstrapOptions: IAngularBootstrapOptions) => {
	// const container = document.getElementById(project.name);
	//
	// container.innerHTML = bootstrapOptions.template;

	return project;
};

window.Aggregator = new Aggregator();