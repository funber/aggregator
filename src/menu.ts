import { Aggregator } from './aggregator';
import { IProject, IProjectMenuItem } from './helpers/project.interface';

// TODO дублирует IProjectMenuItem
export interface IMenuItem  extends IProjectMenuItem {
	projectName: string
}

export class Menu {
	private items: IMenuItem[] = [];

	constructor(
		private aggregator: Aggregator
	) {
		//
	}

	createMenu(projects: IProject[]): Menu {
		// TODO Отсортировать по index
		projects.forEach((project: IProject) => {
			if (project.name !== 'aggregator') {
				this.items.push({
					...<IMenuItem>project.menu,
					projectName: project.name
				})
			}
		});

		return this;
	}

	//TODO Временный метод, хорошо бы задавать его извне, или сделать полностью автономным
	mountMenu(): void {
		const menuContainer: HTMLElement = document.getElementById('main-menu');

		this.items.forEach((item: IMenuItem) => {
			const menuItem: HTMLElement = document.createElement('a');
			menuItem.setAttribute('onclick', 'window.Loader.router.navigateTo(\''+ item.projectName +'\', \''+ item.url +'\')');
			menuItem.innerHTML = item.name;

			menuContainer.appendChild(menuItem);
		})
	}

	getMenu(): IMenuItem[] {
		return this.items;
	}
}